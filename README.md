This is a chatbot built with tensorflow using seq2seq, it's still in its initial foot steps, check out the [dev branch](https://gitlab.com/aliPMPAINT/seq2seq-chatbot/-/tree/dev)

[The course](https://www.udemy.com/course/chatbot/)

[Cornell Movie-Quotes Corpus dataset](https://www.cs.cornell.edu/~cristian/Cornell_Movie-Dialogs_Corpus.html)
